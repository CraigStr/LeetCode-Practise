#
# @lc app=leetcode id=2 lang=python3
#
# [2] Add Two Numbers
#

# @lc code=start
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next


class Solution:
    def addTwoNumbers(self, l1, l2):
        carry = 0
        root = curr_node = ListNode(0)
        while l1 or l2 or carry:
            v1 = v2 = 0

            if l1 != None:
                v1 = l1.val
                l1 = l1.next
            if l2 != None:
                v2 = l2.val
                l2 = l2.next

            carry, m = divmod(v1 + v2 + carry, 10)
            curr_node.next = ListNode(m)
            curr_node = curr_node.next

        return root.next


# @lc code=end