import java.util.Stack;

/*
 * @lc app=leetcode id=20 lang=java
 *
 * [20] Valid Parentheses
 */

// @lc code=start

// Runtime: 1 ms, faster than 99.43% of Java online submissions for Valid Parentheses.
// Memory Usage: 40.7 MB, less than 78.40% of Java online submissions for Valid Parentheses.

class Solution {
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<Character>();

        for (char c : s.toCharArray()) {
            if (c == '[') {
                stack.push(']');
            } else if (c == '{') {
                stack.push('}');
            } else if (c == '(') {
                stack.push(')');
            } else if (stack.isEmpty() || c != stack.pop())
                return false;
        }

        return stack.isEmpty();
    }
}
// @lc code=end

// Runtime: 1 ms, faster than 99.43% of Java online submissions for Valid
// Parentheses.
// Memory Usage: 42.5 MB, less than 18.02% of Java online submissions for Valid
// Parentheses.

// class Solution {
// public boolean isValid(String s) {
// char[] stack = new char[s.length()];
// int stackTop = -1;

// for (char c : s.toCharArray()) {
// if (c == '[') {
// stackTop++;
// stack[stackTop] = ']';
// } else if (c == '{') {
// stackTop++;
// stack[stackTop] = '}';
// } else if (c == '(') {
// stackTop++;
// stack[stackTop] = ')';
// }
// else {
// if (stackTop == -1) return false;
// if (c != stack[stackTop]) return false;
// stackTop--;
// }
// }

// return stackTop == -1;
// }
// }