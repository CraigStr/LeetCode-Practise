#
# @lc app=leetcode id=53 lang=python3
#
# [53] Maximum Subarray
#

from typing import List

# @lc code=start

class Solution:
    # O(2n)
    def maxSubArray(self, nums: List[int]) -> int:
        for i in range(1, len(nums)): # O(n)
            if nums[i - 1] > 0:
                nums[i] += nums[i - 1]
        return max(nums) # O(n)
        
# @lc code=end

sol = Solution()
print(
    sol.maxSubArray([-2,1,-3,4,-1,2,1,-5,4]),
    sol.maxSubArray([5,4,-1,7,8])
)