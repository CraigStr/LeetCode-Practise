#
# @lc app=leetcode id=242 lang=python3
#
# [242] Valid Anagram
#

from collections import defaultdict

# @lc code=start
class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False
        
        count = defaultdict(int)

        for c in s:
            count[c] += 1
        
        for c in t:
            count[c] -= 1
        return all(x == 0 for x in count.values())
        
# @lc code=end

sol = Solution()
print(sol.isAnagram("nagaram", "anagram"))
print(sol.isAnagram("a", "b"))