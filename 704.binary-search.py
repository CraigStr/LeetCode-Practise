#
# @lc app=leetcode id=704 lang=python3
#
# [704] Binary Search
#
# @lc code=start

# Accepted
# 47/47 cases passed (240 ms)
# Your runtime beats 92.42 % of python3 submissions
# Your memory usage beats 81.7 % of python3 submissions (15.5 MB)

class Solution:
    def search(self, nums, target: int) -> int:
        l = 0
        r = len(nums) - 1

        # Number must be within these bounds to be in sorted list
        if target < nums[l] or target > nums[r]:
            return -1

        while l <= r:
            pivot = (r + l) // 2
            pivot_val = nums[pivot]

            if pivot_val == target:
                return pivot
            if pivot_val > target:
                r = pivot - 1
            else:
                l = pivot + 1
        return -1

# @lc code=end


# -------------------------------------------------

# Accepted
# 47/47 cases passed (350 ms)
# Your runtime beats 44.15 % of python3 submissions
# Your memory usage beats 81.7 % of python3 submissions (15.5 MB)

# class Solution:
#     def search(self, nums, target: int) -> int:
#         l = 0
#         r = len(nums) - 1

#         # Number must be within these bounds to be in sorted list
#         if target < nums[l] or target > nums[r]:
#             return -1

#         while l <= r:
#             pivot = (r + l) // 2
#             pivot_val = nums[pivot]

#             if pivot_val == target:
#                 return pivot
#             if pivot_val > target:
#                 r = pivot - 1
#             else:
#                 l = pivot + 1
#         return -1

# -------------------------------------------------

# Accepted
# 47/47 cases passed (375 ms)
# Your runtime beats 32.77 % of python3 submissions
# Your memory usage beats 87.59 % of python3 submissions (15.5 MB)

# class Solution:
#     def search(self, nums, target: int) -> int:
#         l = 0
#         r = len(nums) - 1

#         while l <= r:
#             pivot = (r + l) // 2
#             pivot_val = nums[pivot]

#             if pivot_val == target:
#                 return pivot
#             if pivot_val > target:
#                 r = pivot - 1
#             else:
#                 l = pivot + 1
#         return -1

# -------------------------------------------------

# Accepted
# 47/47 cases passed (468 ms)
# Your runtime beats 8.81 % of python3 submissions
# Your memory usage beats 87.59 % of python3 submissions (15.4 MB)


# class Solution:
#     def search(self, nums, target: int) -> int:
#         l = 0
#         r = len(nums) - 1

#         while l <= r:
#             pivot = (r + l) // 2
#             pivot_val = nums[pivot]

#             if pivot_val == target:
#                 return pivot
#             if pivot_val > target:
#                 r = pivot - 1
#             else:
#                 l = pivot + 1
#         return -1

# -------------------------------------------------

# Accepted
# 47/47 cases passed (412 ms)
# Your runtime beats 21.37 % of python3 submissions
# Your memory usage beats 57.35 % of python3 submissions (15.5 MB)


# class Solution:
#     def search(self, nums, target: int) -> int:
#         pointer_l = 0
#         pointer_r = len(nums) - 1
#         pointer_c = (pointer_r - pointer_l) // 2

#         while pointer_l <= pointer_r:
#             if nums[pointer_c] == target:
#                 return pointer_c
#             if nums[pointer_c] > target:
#                 pointer_r = pointer_c - 1
#                 pointer_c = (pointer_r + pointer_l) // 2
#             if nums[pointer_c] < target:
#                 pointer_l = pointer_c + 1
#                 pointer_c = (pointer_r + pointer_l) // 2
#         return -1

# -------------------------------------------------
