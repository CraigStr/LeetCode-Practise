#
# @lc app=leetcode id=15 lang=python3
#
# [15] 3Sum
#

# @lc code=start
from typing import List


class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        nums.sort()
        
        # If minimum is > 0, then we can never reach 0
        if len(nums) == 0 or nums[0] > 0:
            return []
        
        res = []
        
        for i in range(len(nums) - 2): # Dont need to iterate over the last 2 items
            
            # Don't check i duplicates
            if i > 0 and nums[i] == nums[i - 1]:
                continue
            
            l = i + 1
            r = len(nums) - 1
            
            while l < r:
                total = nums[i] + nums[l] + nums[r] 
                
                if total == 0:
                    res.append([nums[i], nums[l], nums[r]])
                    # Dont check l duplicates
                    while l < r and nums[l] == nums[l + 1]:
                        l += 1
                    # Dont check r duplicates
                    while l < r and nums[r] == nums[r - 1]:
                        r -= 1
                    l += 1
                    r -= 1
                elif total < 0:
                    l += 1
                else:
                    r -= 1
        return res
        
# @lc code=end

sol = Solution()
print(
    sol.threeSum([1,-1,-1,0])
)
