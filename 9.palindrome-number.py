#
# @lc app=leetcode id=9 lang=python3
#
# [9] Palindrome Number
#

# @lc code=start

# -------------------------------------------------

"""
Using String Conversion

Accepted
11510/11510 cases passed (64 ms)
Your runtime beats 90.82 % of python3 submissions
Your memory usage beats 91.22 % of python3 submissions (13.8 MB)

Space Complexity: O(N)
Time Complexity: O(N)
"""


class Solution:
    def isPalindrome(self, x: int) -> bool:
        s = str(x)
        return s == s[::-1]

# -------------------------------------------------


"""
Follow Up
Could you solve it without converting the integer to a string?

Accepted
11510/11510 cases passed (96 ms)
Your runtime beats 39.41 % of python3 submissions
Your memory usage beats 91.22 % of python3 submissions (13.8 MB)

Space Complexity: O(log10(N))
Time Complexity: O(1)
"""


class Solution:
    def isPalindrome(self, x: int) -> bool:

        # Numbers ending in 0 must start with 0 to be palindrome,
        # therefore 0 is the only palindrome divisible by 10
        if x % 10 == 0 and x != 0:
            return False

        rev = 0
        while x > rev:
            x, m = divmod(x, 10)
            rev = (rev * 10) + m
        return rev == x or rev//10 == x

# -------------------------------------------------


# @lc code=end
