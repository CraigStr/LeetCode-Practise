#
# @lc app=leetcode id=977 lang=python3
#
# [977] Squares of a Sorted Array
#

from typing import List

# @lc code=start

# -------------------------------------------------

# Approach using two pointers


class Solution:
    def sortedSquares(self, nums: List[int]) -> List[int]:
        l = 0
        r = len(nums) - 1
        a = [0] * len(nums)

        while l <= r:
            l_val = abs(nums[l])
            r_val = abs(nums[r])

            if l_val > r_val:
                a[r-l] = l_val ** 2
                l += 1
            else:
                a[r-l] = r_val ** 2
                r -= 1
        return a

# -------------------------------------------------


# @lc code=end

sol = Solution()
print(sol.sortedSquares([-10000, -9999, -7, -5, 0, 0, 10000]))
