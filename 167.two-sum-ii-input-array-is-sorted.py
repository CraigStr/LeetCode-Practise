#
# @lc app=leetcode id=167 lang=python3
#
# [167] Two Sum II - Input Array Is Sorted
#

from typing import List

# @lc code=start


class Solution:
    def twoSum(self, numbers: List[int], target: int) -> List[int]:
        l_p = 0
        r_p = len(numbers) - 1

        while l_p < r_p:
            sum = numbers[l_p] + numbers[r_p]
            if sum == target:
                return [l_p + 1, r_p + 1]
            if sum < target:
                l_p += 1
            else:
                r_p -= 1

# @lc code=end


sol = Solution()
print(sol.twoSum([2, 7, 11, 15], 18))
