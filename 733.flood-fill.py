#
# @lc app=leetcode id=733 lang=python3
#
# [733] Flood Fill
#

from typing import List

# @lc code=start


class Solution:

    def dfs(self, image, sr, sc, start_color, new_color):
        if 0 > sr or sr >= len(image) or 0 > sc or sc >= len(image[0]) or image[sr][sc] != start_color or image[sr][sc] ==  new_color:
            return
        image[sr][sc] = new_color
        self.dfs(image, sr + 1, sc, start_color, new_color)
        self.dfs(image, sr - 1, sc, start_color, new_color)
        self.dfs(image, sr, sc + 1, start_color, new_color)
        self.dfs(image, sr, sc - 1, start_color, new_color)

    def floodFill(self, image: List[List[int]], sr: int, sc: int, new_color: int) -> List[List[int]]:

        start_color = image[sr][sc]
        self.dfs(image, sr, sc, start_color, new_color)
        return image

# @lc code=end


sol = Solution()
print(
    sol.floodFill([[1]], 0, 0, 2)
)
