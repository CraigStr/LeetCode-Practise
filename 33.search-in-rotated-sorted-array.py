#
# @lc app=leetcode id=33 lang=python3
#
# [33] Search in Rotated Sorted Array
#

# @lc code=start
from typing import List


class Solution:
    def search(self, nums: List[int], target: int) -> int:
        # Find index of smallest
        l = 0
        r = len(nums) - 1

        while l < r:
            c = (l + r) // 2
            if nums[c] > nums[r]:
                l = c + 1
            else:
                r = c

        # l == r is at smallest index
        rot = l

        while l <= r:
            c = (l + r) // 2
            rot_c = (c + rot) % len(nums)
            if nums[rot_c] == target:
                return rot_c
            elif nums[rot_c] < target:
                l = c + 1
            else:
                r = c - 1
        return -1

# @lc code=end


sol = Solution()
print(
    sol.search([4, 5, 6, 7, 0, 1, 2], 0)
)
