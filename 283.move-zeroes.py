#
# @lc app=leetcode id=283 lang=python3
#
# [283] Move Zeroes
#

from typing import List


# @lc code=start


class Solution:
    def moveZeroes(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        n_p = 0
        for i in range(len(nums)):
            if nums[i] != 0:
                nums[n_p], nums[i] = nums[i], nums[n_p]
                n_p += 1

# @lc code=end


# Accepted
# 74/74 cases passed (354 ms)
# Your runtime beats 19.99 % of python3 submissions
# Your memory usage beats 34.51 % of python3 submissions (15.5 MB)


# class Solution:
#     def moveZeroes(self, nums: List[int]) -> None:
#         """
#         Do not return anything, modify nums in-place instead.
#         """
#         z_p = 0
#         n_p = 0

#         while n_p < len(nums):
#             if nums[z_p] == nums[n_p] == 0:
#                 n_p += 1
#             elif nums[z_p] == 0 and nums[n_p] != 0:
#                 nums[z_p], nums[n_p] = nums[n_p], nums[z_p]
#                 z_p += 1
#                 n_p += 1
#             else:
#                 z_p += 1
#                 n_p += 1

        return nums


sol = Solution()
print(sol.moveZeroes([0, 0, 0, 0, 0, 1]))
