#
# @lc app=leetcode id=35 lang=python3
#
# [35] Search Insert Position
#

# @lc code=start

# Accepted
# 64/64 cases passed (48 ms)
# Your runtime beats 88.49 % of python3 submissions
# Your memory usage beats 96.32 % of python3 submissions (14.7 MB)

class Solution:
    def searchInsert(self, nums, target: int) -> int:
        l = 0
        r = len(nums)

        while l < r:
            pivot = (l + r) // 2
            pivot_val = nums[pivot]
            if pivot_val == target:
                return pivot
            if target > pivot_val:
                l = pivot + 1
            else:
                r = pivot

        return l

# @lc code=end

# -------------------------------------------------

# Accepted
# 64/64 cases passed (93 ms)
# Your runtime beats 14.14 % of python3 submissions
# Your memory usage beats 84.1 % of python3 submissions (14.8 MB)


# class Solution:
#     def searchInsert(self, nums, target: int) -> int:
#         l = 0
#         r = len(nums) - 1
#         pivot = 0

#         while l < r:
#             pivot = (l + r) // 2
#             pivot_val = nums[pivot]
#             if pivot_val == target:
#                 return pivot
#             if target > pivot_val:
#                 l = pivot + 1
#             else:
#                 r = pivot

#         return l

# -------------------------------------------------

# Accepted
# 64/64 cases passed (68 ms)
# Your runtime beats 47.66 % of python3 submissions
# Your memory usage beats 69.61 % of python3 submissions (14.9 MB)

# class Solution:
#     def searchInsert(self, nums, target: int) -> int:
#         l = 0
#         r = len(nums) - 1
#         pivot = 0

#         while l <= r:
#             pivot = (l + r) // 2

#             if nums[pivot] == target:
#                 return pivot
#             if target > nums[pivot]:
#                 l = pivot + 1
#             else:
#                 r = pivot - 1

#         # If no element,
#         # Last pivot position will either be:
#         #   the nearest larger number
#         #   the nearest smaller number
#         # If nearest larger number,
#         #   inserting the target will insert at pivot
#         # If nearest smaller number,
#         #   the target will be inserted after pivot
#         return [pivot if nums[pivot] > target else pivot + 1][0]

# -------------------------------------------------
