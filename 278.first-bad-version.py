#
# @lc app=leetcode id=278 lang=python3
#
# [278] First Bad Version
#

# @lc code=start

# The isBadVersion API is already defined for you.
# def isBadVersion(version: int) -> bool:

# -------------------------------------------------

# Accepted
# 22/22 cases passed (32 ms)
# Your runtime beats 76.13 % of python3 submissions
# Your memory usage beats 90.88 % of python3 submissions (13.9 MB)

class Solution:
    def firstBadVersion(self, n: int) -> int:
        l = 0

        while l <= n:
            pivot = (n + l) // 2
            pivot_val = isBadVersion(pivot)

            if pivot_val and (not isBadVersion(pivot-1)):
                return pivot
            if pivot_val:
                n = pivot - 1
            else:
                l = pivot + 1
        return -1

# @lc code=end


# -------------------------------------------------

# Accepted
# 22/22 cases passed (44 ms)
# Your runtime beats 39.82 % of python3 submissions
# Your memory usage beats 90.88 % of python3 submissions (13.8 MB)


# class Solution:
#     def firstBadVersion(self, n: int) -> int:
#         l = 0

#         while l <= n:
#             pivot = (n + l) // 2

#             if isBadVersion(pivot) and (not isBadVersion(pivot-1)):
#                 return pivot
#             if isBadVersion(pivot):
#                 n = pivot - 1
#             else:
#                 l = pivot + 1
#         return -1

# -------------------------------------------------
