#
# @lc app=leetcode id=344 lang=python3
#
# [344] Reverse String
#

from typing import List

# @lc code=start

# Accepted
# 477/477 cases passed (228 ms)
# Your runtime beats 62.61 % of python3 submissions
# Your memory usage beats 71.32 % of python3 submissions (18.4 MB)

class Solution:
    def reverseString(self, s: List[str]) -> None:
        """
        Do not return anything, modify s in-place instead.
        """

        l = 0
        r = len(s) - 1

        while l < r:
            s[l], s[r] = s[r], s[l]
            l += 1
            r -= 1

# @lc code=end

        return s


sol = Solution()
print(sol.reverseString(["h", "e", "l", "l", "o", "s"]))
