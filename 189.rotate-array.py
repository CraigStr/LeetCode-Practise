#
# @lc app=leetcode id=189 lang=python3
#
# [189] Rotate Array
#

from typing import List

# @lc code=start

# Accepted
# 38/38 cases passed (249 ms)
# Your runtime beats 69.05 % of python3 submissions
# Your memory usage beats 88.77 % of python3 submissions (25.3 MB)

class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        # Prevent excessive use of len()
        size = len(nums)

        # If k > size,
        # there will be a number < size
        # which results in the same rotation
        k = k % size

        # Reversing first n - k - 1  numbers
        l = 0
        r = size - k - 1
        while l < r:
            nums[l], nums[r] = nums[r], nums[l]
            r -= 1
            l += 1
        # Reversing last k numbers
        l = size - k
        r = size - 1
        while l < r:
            nums[l], nums[r] = nums[r], nums[l]
            r -= 1
            l += 1
        # Reverse whole array
        l = 0
        r = size - 1
        while l < r:
            nums[l], nums[r] = nums[r], nums[l]
            r -= 1
            l += 1

# @lc code=end

        return nums


sol = Solution()
print(sol.rotate([1, 2, 3], 4))
