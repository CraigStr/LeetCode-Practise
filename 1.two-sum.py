#
# @lc app=leetcode id=1 lang=python3
#
# [1] Two Sum
#

# @lc code=start

# -------------------------------------------------

# Accepted
# 57/57 cases passed (3462 ms)
# Your runtime beats 28.12 % of python3 submissions
# Your memory usage beats 85.95 % of python3 submissions (14.9 MB)

# # Space Complexity: O(1)
# # Time Complexity: O(N^2)

class Solution:
    def twoSum(self, nums, target):
        for i, i_item in enumerate(nums):
            for j, j_item in enumerate(nums[i+1::]):
                if i_item + j_item == target:
                    return i, j + i + 1

# -------------------------------------------------
# Accepted
# 57/57 cases passed (9309 ms)
# Your runtime beats 5 % of python3 submissions
# Your memory usage beats 85.95 % of python3 submissions (14.8 MB)

# # Space Complexity: O(1)
# # Time Complexity: O(N^2)
# class Solution:
#     def twoSum(self, nums, target):
#         for i in range(len(nums)):
#             for j in range(len(nums[i + 1::])):
#                 if nums[i] + nums[j + i + 1] == target:
#                     return i, j + i + 1

# -------------------------------------------------

# @lc code=end